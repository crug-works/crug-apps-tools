# CRUG Apps and Tools

![CRUG logo](crug_skyline.png)

## Repository for CRUG desktop and web apps and tools across domains:

- ### Business, Economics, and Finance
- ### Humanities (Arts, Literature, Language, Philosophy)
- ### Life Sciences (Biology, Ecology, Environment)
- ### Physical Sciences (Astronomy, Chemistry, Physics)
- ### Social Sciences (Anthropology, Political Science, Sociology)
- ### Government (Budget, Defense, Law)

